from flask import Flask
from flask import Flask,redirect,render_template
from flask import request
import json
from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse, Say,Gather,Hangup
import time
import os
from json import dumps
import urllib
import logging
import pandas as pd
_logger = logging.getLogger(__name__)

import platform
if platform.system() == 'Windows':
    df = pd.read_excel(r"C:\Users\Alberto\PycharmProjects\StatCallCenter\StatCall\StatCall\data\pro_02.xlsx")
else:
    df = pd.read_excel("pro_02.xlsx")

URL_HTTPS_SERVER = "http://34.67.71.180:80/"
app = Flask(__name__)
#LIST_OF_PHONE = ['+18496426116','+18096694766']
LIST_OF_PHONE = ['+18295572357']

"""
#free account
account_sid = "ACa73d86f612992eceb179637a22e390a9"
auth_token = "d4f5003c622278a67e0cd2d23c647ce4"
from_ = "+12092317536",

"""
#intelca account
account_sid = "AC6b096b20ec678d84062af773d91a8b2b"
auth_token = "dacf78473a323394e8e192321a1c6ba9"
from_ = "+14422455570",
"""
for index, row in df.head().iterrows():
    # access data using column names
    print(index, row['nombres'], row['candidato'], row['tel'])
    phone = str(row['tel'])
    phone = phone.replace("(","")
    phone = phone.replace(")", "")
    phone = phone.replace("-", "")
    print(phone)
"""

@app.route('/')
def index():

    return render_template('index.html')


@app.route("/create_call/", methods=['POST'])
def start_stat():
    #phone from the list to be called
    #_logger.info("Start Call Poll Elections")
    data_url ={
        'phone':0,
        'vote':-1,
        'max_phone_calls':1

     }
    data = urllib.parse.urlencode(data_url)

    return  redirect('/repeat_call/?'+ data )


@app.route("/repeat_call/", methods=['GET'])
def repeat_call_func():
    #phone from the list to be called

    # iterate over rows with iterrows()
    for index, row in df.head().iterrows():
        # access data using column names
        print(index, row['nombres'], row['candidato'], row['tel'])
        phone = str(row['tel'])
        phone = phone.replace("(", "")
        phone = phone.replace(")", "")
        phone = phone.replace("-", "")

        data_url = {
            'phone': row['tel'],
            'vote': request.args.get('vote'),
            'max_phone_calls': request.args.get('max_phone_calls')

        }
        print("index values: ",index)

        if row['candidato'] =='nan':

            # print and log data
            print("Call to " + phone)
            _logger.info("Start Call Poll Elections for " + phone)

            data = urllib.parse.urlencode(data_url)
            client = Client(account_sid, auth_token)
            call = client.calls.create(
                to=phone,
                from_=from_,
                url=URL_HTTPS_SERVER + "plc/xml/?" + data,
                method="GET",
                status_callback=URL_HTTPS_SERVER + "call_status/?" + data,
                status_callback_method="GET",
                status_callback_event=["initiated", "ringing", "answered", "completed"]

            )
            _logger.info("Start Calling " + phone)
            call.sid

            return (call.sid)
    return "hello"


@app.route('/plc/xml/',methods=['GET'])
def plc_xml_twilio_message():
    data_url ={
        'phone':request.args.get('phone'),
        'vote':request.args.get('vote'),
        'max_phone_calls':request.args.get('max_phone_calls')
     }
    phone = request.args.get('phone')
    _logger.info("Gather Data for " + phone)

    data = urllib.parse.urlencode(data_url)

    resp = VoiceResponse()

    # Start our <Gather> verb
    resp.pause(length=1)
    resp.say('ENCUESTA PARA ELECCIONES 2020', language='es-DO',voice='Polly.Penelope')
    resp.say('Por favor con su voz,diga el nombre de su candidato presidencial preferido', language='es-DO',
             voice='Polly.Penelope')

    gather = Gather(timeout=25,input='speech',language='es-DO',speechModel="default",
                    method='GET', hints="gonzalo,castillo,luis,Abinader,leonel,fernanzez",
                    action='/process_gather/?'+data)


    #gather.say('Presione 2 por Luis Abinader Corona', language='es')

    resp.append(gather)
    resp.hangup()

    return str(resp)


@app.route('/process_gather/',methods=['GET'])
def plc_process_gather():


    """Respond to incoming phone calls with a menu of options"""
    # Start our TwiML response
    resp = VoiceResponse()
    # If Twilio's request to our app included already gathered digits,
    # process them
        # Get which digit the caller chos]
    digit = request.args.get('Digits')
    speech = request.args.get('SpeechResult')
    phone = request.args.get('phone')
    call_status = request.args.get('CallStatus')
    print("Call data for spech  " + phone + "is " + str(speech))
    if  speech:
        # Get which digit the caller chose
        df.loc[df['tel'] == phone, 'candidato'] = speech
        df.to_excel("pro_02.xlsx")


        # save stat in txt file sta.txt
        f = open('stat.txt', 'a')
        f.write(phone + " , " + call_status +" , "+ speech + "\n")
        f.close()
        resp.say('Gracias', language='es',voice='Polly.Penelope')
        resp.hangup()
        return str(resp)

    else:
        # save stat in txt file sta.txt
        f = open('stat.txt', 'a')
        f.write(phone + " , " + call_status +" , "+ speech + "\n")
        f.close()
        # If the caller didn't choose 1 or 2, apologize and ask them again
        #resp.say("Ninguna entrada valida detectada.",language='es')
        return str(resp)

    return str(resp)


@app.route('/call_status/',methods=['GET','POST'])
def plc_call_status():
    """Respond to incoming phone calls with a menu of options"""
    call_status = request.args.get('CallStatus')
    phone = request.args.get('phone')
    if request.args.get('SpeechResult'):
        speech = request.args.get('SpeechResult')
    else:
        speech ='No data'
    resp = VoiceResponse()

    print("CallStatus  " + phone + " is " +  call_status + str(speech))

    if call_status == "completed":
        print("Call Status Complete")
        resp.hangup()



    return str(resp)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
    #app.run()











