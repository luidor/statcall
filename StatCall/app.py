from flask import Flask,request, jsonify
from flask import Flask, redirect, render_template
from flask import request
import json
from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse, Say, Gather, Hangup
import time
import os
from json import dumps
import urllib
import logging
import pandas as pd
_logger = logging.getLogger(__name__)
import platform

URL_HTTPS_SERVER = "http://34.67.71.180:80/"
FILE_TEST = "data_test.xlsx"
LIMI_DATA =1
LIST_OF_PHONE =[]


if platform.system() == 'Windows':
    df = pd.read_excel(r"C:\Users\Alberto\PycharmProjects\StatCallCenter\StatCall\StatCall\data_test.xlsx")
else:
    df = pd.read_excel(FILE_TEST)

app = Flask(__name__)

"""
#free account
account_sid = "ACa73d86f612992eceb179637a22e390a9"
auth_token = "d4f5003c622278a67e0cd2d23c647ce4"
from_ = "+12092317536",

"""
# intelca account
account_sid = "AC6b096b20ec678d84062af773d91a8b2b"
auth_token = "dacf78473a323394e8e192321a1c6ba9"
from_ = "+14422455570",


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/set_calls/',methods=['GET', 'POST'])
def set_calls():

    file = open("stat.txt", "w")
    file.close()
    file = open("stat.txt", "r+")
    file.truncate(0)
    file.close()
    content = request.json
    print (content['name'])
    print(content['phone_list'])
    LIST_OF_PHONE = content['phone_list']
    for phone in LIST_OF_PHONE:
        data_url = {
            'phone': phone,
            'vote': request.args.get('vote'),
            'max_phone_calls': request.args.get('max_phone_calls')

        }
        # print and log data
        print("Call to " + phone)
        _logger.info("Start Call Poll Elections for " + phone)

        data = urllib.parse.urlencode(data_url)
        client = Client(account_sid, auth_token)
        call = client.calls.create(
            to=phone,
            from_=from_,
            url=URL_HTTPS_SERVER + "plc/xml/?" + data,
            method="GET",
            status_callback=URL_HTTPS_SERVER + "call_status/?" + data,
            status_callback_method="GET",
            status_callback_event=["initiated", "ringing", "answered", "completed"]

        )
        _logger.info("Start Calling " + phone)
        call.sid
    return (call.sid)







@app.route('/get_calls_data/', methods=['GET', 'POST'])
def get_calls_data():
    # open('stat.txt', 'w').close()
    filepath = 'stat.txt'
    data_list =[]
    with open(filepath) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            print("phone:  {}: {}".format(cnt, line.strip()))
            data_list.append("{}: {}".format(cnt, line.strip()))
            line = fp.readline()
            cnt += 1
    data={"data_list":data_list}
    return jsonify(data)







@app.route('/plc/xml/', methods=['GET'])
def plc_xml_twilio_message():
    data_url = {
        'phone': request.args.get('phone'),
        'vote': request.args.get('vote'),
        'max_phone_calls': request.args.get('max_phone_calls')
    }
    phone = request.args.get('phone')
    _logger.info("Gather Data for " + phone)

    data = urllib.parse.urlencode(data_url)

    resp = VoiceResponse()

    # Start our <Gather> verb
    resp.pause(length=1)
    resp.say('ENCUESTA PARA ELECCIONES 2020, Seran solo dos preguntas', language='es-DO',voice='Polly.Penelope')
    resp.say('Por favor diga con su voz,Por quien votará en las elecciones del 5 de Julio, despues del tono', language='es-DO',
             voice='Polly.Penelope')
    resp.play("https://www.soundjay.com/button/sounds/beep-01a.mp3")

    gather = Gather(input='speech',language='es-DO',speechModel="default",speechTimeout=2,
                    method='GET', hints="Gonzalo,castillo,luis,Abinader,leonel,fernanzez",
                    action='/process_gather_2016/?'+data)
    resp.append(gather)



    resp.hangup()

    return str(resp)


@app.route('/process_gather/', methods=['GET'])
def plc_process_gather():
    """Respond to incoming phone calls with a menu of options"""
    # Start our TwiML response
    resp = VoiceResponse()
    # If Twilio's request to our app included already gathered digits,
    # process them
    # Get which digit the caller chos]
    digit = request.args.get('Digits')
    speech = request.args.get('SpeechResult')
    phone = request.args.get('phone')
    call_status = request.args.get('CallStatus')
    print("Call data for spech  " + phone + "is " + str(speech))
    if speech:
        # Get which digit the caller chose
        resp.say('Gracias', language='es', voice='Polly.Penelope')
        resp.hangup()
        #df.loc[df['fix_tel'] == phone, 'voto'] = speech

        #df.loc[df['tel'] == phone.replace("+1", ""), 'voto'] = speech
        #df.to_excel("data_test.xlsx", index_label=False, index=False, header=True)

        # save stat in txt file sta.txt
        f = open('stat.txt', 'a')
        f.write(phone + " , " + call_status + " , " + "voto 2016 por " + speech + "\n")
        f.close()

        return str(resp)

    else:
        # save stat in txt file sta.txt
        f = open('stat.txt', 'a')
        f.write(phone + " , " + call_status + " , " + speech + "\n")
        f.close()
        # If the caller didn't choose 1 or 2, apologize and ask them again
        # resp.say("Ninguna entrada valida detectada.",language='es')
        resp.say('Gracias', language='es', voice='Polly.Penelope')
        resp.hangup()
        return str(resp)

    return str(resp)


@app.route('/process_gather_2016/', methods=['GET'])
def plc_process_gather_2016():
    """Respond to incoming phone calls with a menu of options"""

    data_url = {
        'phone': request.args.get('phone'),
        'vote': request.args.get('vote'),
        'max_phone_calls': request.args.get('max_phone_calls')
    }
    # Start our TwiML response
    resp = VoiceResponse()
    data = urllib.parse.urlencode(data_url)

    resp.say('Por favor diga con su voz,Por quien votó en las elecciones del 2016, despues del tono',
             language='es-DO',
             voice='Polly.Penelope')

    resp.play("https://www.soundjay.com/button/sounds/beep-01a.mp3")

    gather_1 = Gather(speechTimeout=2, input='speech', language='es-DO', speechModel="default",
                      method='GET', hints="danilo,medina,luis,Abinader,leonel,fernanzez",
                      action='/process_gather/?' + data)
    # gather.say('Presione 2 por Luis Abinader Corona', language='es')

    resp.append(gather_1)






    # If Twilio's request to our app included already gathered digits,
    # process them
    # Get which digit the caller chos]
    digit = request.args.get('Digits')
    voto_2016 = request.args.get('SpeechResult')
    phone = request.args.get('phone')
    call_status = request.args.get('CallStatus')
    print("Call data for spech  " + phone + "is " + str(voto_2016))


    if voto_2016:



        # save stat in txt file sta.txt
        f = open('stat.txt', 'a')
        f.write(phone + " , " + call_status + " , voto 2020 por  " + voto_2016 + "\n")
        f.close()

        return str(resp)

    else:
        # save stat in txt file sta.txt
        f = open('stat.txt', 'a')
        f.write(phone + " , " + call_status + " , " + speech + "\n")
        f.close()
        # If the caller didn't choose 1 or 2, apologize and ask them again
        # resp.say("Ninguna entrada valida detectada.",language='es')
        resp.say('Gracias', language='es', voice='Polly.Penelope')

        return str(resp)

    return str(resp)






@app.route('/call_status/', methods=['GET', 'POST'])
def plc_call_status():
    """Respond to incoming phone calls with a menu of options"""
    call_status = request.args.get('CallStatus')
    phone = request.args.get('phone')
    if request.args.get('SpeechResult'):
        speech = request.args.get('SpeechResult')
    else:
        speech = 'No data'
    resp = VoiceResponse()

    print("CallStatus  " + phone + " is " + call_status + str(speech))

    if call_status == "completed":
        print("Call Status Complete")
        if not(request.args.get('SpeechResult')):
            f = open('stat.txt', 'a')
            f.write(phone + " , " + call_status + " , " + speech + "\n")
            f.close()

        resp.hangup()

    return str(resp)


if __name__ == '__main__':
    #data from excell
    """
    for index, row in df.iterrows():
        # access data using column names
        #print(index, row['nombres'], row['candidato'], row['tel'])

        phone_temp_1 = str(row['fix_tel'])
        phone_temp = str(row['tel'])
        phone_temp_1 = phone_temp_1.replace(".0", "")
        phone_temp = phone_temp.replace(".0", "")
        #phone_temp = phone_temp.replace(")", "")
        #phone_temp = phone_temp.replace("-", "")
        phone_temp = "+" + phone_temp_1
        LIST_OF_PHONE.append(phone_temp)

        #df.loc[df['tel'] == row['tel'], 'fix_tel'] = phone_temp

        #df.to_excel("data_test.xlsx",index_label=False, index=False, header=True)

        if index == LIMI_DATA:
            break

    print(LIST_OF_PHONE)
    """

    app.run(host='0.0.0.0', port=80)
    # app.run()











